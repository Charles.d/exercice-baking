# Exercice pour le poste de Head of Node
## Installation de Tezos
Pour récupérer tezos, j'ai utilisé git 

```git clone https://gitlab.com/tezos/tezos```

Puis j'ai procédé à une compilation du dépôt

```sh
cd tezos;
make build-deps;
eval $(opam env);
make
cd -
```

Pour le démarage des nœuds, j'ai commencé par télécharger un snapshot :

```sh
wget https://ghostnet.xtz-shots.io/rolling -O tezos-ghostnet.rolling
```

puis j'ai créé une fonction permettant de configurer un nœud en fonction de son numéro : 

## Démarage de trois nœuds

```sh
run_node(){
	reseau=${2-ghostnet}
	num=$1
	dir_snapshots=~
	dir=/home/ubuntu/noeuds/$reseau/$num
	logs=$dir/logs
	rpc_client=[::]:973$num
	addr=[::]:972$num
	metrics=[::]:974$num
	mkdir -p $dir_snapshots
	mkdir -p $dir


	echo octez-node config init --data-dir=$dir --network $reseau --rpc-addr=$rpc --allow-all-rpc=$rpc --net-addr=$addr --metrics-addr=$metrics
	echo octez-node snapshot import $dir_snapshots/tezos-$reseau.rolling --data-dir=$dir
	echo octez-node run --data-dir=$dir --allow-all-rpc=$rpc --rpc-addr $rpc --log-output=$logs --net-addr=$addr --metrics-addr=$metrics ;}
```


Enfin dans trois threads différents, j'ai lancé les nœuds

```sh
run_node 1 &
run_node 2 &
run_node 3 &
```

## Baker
afin de démarer le baker, j'ai tout d'abord créé un nouveau nœud

```sh

octez-node run --data-dir=/home/ubuntu/noeuds/ghostnet --allow-all-rpc=9731 --rpc-addr localhost:9731 --log-output=/home/ubuntu/noeuds/ghostnet/logs --net-addr=[::]:9721 --metrics-addr=[::]:9741

```

J'ai ensuite configuré le client pour qu'il se connecte à ce nœud

```sh
octez-client --endpoint http://localhost:9731 bootstrapped
octez-client --endpoint http://localhost:9731 config update
```

Puis, j'ai créé une paire de clefs

```sh
octez-client gen keys baker
```
J'ai obtenu la clef `tz1baker3x4XiTAigp2UjoSnbFXFyegU8SWT`
(Pour la regénération, il faut utiliser
```sh
octez-client import secret key baker unencrypted:edsk3Sv8agXo5ekiCVoyi69PT8s5kCMTxwgahES6eBZ9ZP8L7zBa8T
```


En suite j'envoie un nombre suffisant de tez à cette adresse pour
lui permettre de baker avec une fréquence correcte grâce au faucet de ghostnet ( https://faucet.ghostnet.teztnets.xyz )

Puis, je l'enregistre comme délégué

```sh
octez-client register key baker
```


*Pour une raison étrange, cette commande ne fonctionne pas, pour remédier à ce problème j'utilise cette autre commande pour obterir le même résultat :*
```sh
octez-client set delegate for baker to baker
```

Je démare le démon de baking
```sh
octez-baker-PtMumbai run with local node /home/ubuntu/noeuds/ghostnet --liquidity-baking-toggle-vote on
```

normalement, à partir d'ici, il faut attendre environ 3 jours.

## netdata
Je crée un compte sur [netdata](netdata.cloud).
Je lance la commande dédiée sur leur site.

Les graphiques sont disponibles sur mon espace netdata

## grafana
Tout d'abord, il faut installer Grafana. 
Pour cela, j'ai suivi la procédure recommandé sur le site https://grafana.com/docs/grafana/latest/setup-grafana/installation/debian/

### prometheus
Il faut en suite configurer prometheus en ajoutant le code suivant à la fin du fichier `/etc/prometheus/prometheus.yml`

```yml
  - job_name: 'octez-exporter'
     scrape_interval: interval s
     metrics_path: "/metrics"
     static_configs:
       - targets: ['0.0.0.0:8733']
```

Il faut ensuite démarer Prometheus
```sh
sudo systemctl start prometheus
```

On importe loki et promtail aux adresses indiquées :
- https://github.com/grafana/loki/releases/
- https://sbcode.net/grafana/install-promtail-service/

et on les lance :

```sh
./loki-linux-amd64 -config.file=loki-local-config.yaml &
./promtail-linux-amd64 -config.file=promtail-local-config.yaml &
```
enfin, on récupère grafazos et on l'exécute :
```sh
git clone https://gitlab.com/nomadic-labs/grafazos.git
cd grafazos
make all
```

on démare alors le serveur grafana avec la commande gra

```sh
sudo systemctl start grafana-server.service 
sudo grafana server
```
Puis on se connecte au serveur sur le port 3000 et on peut configurer les paneaux

Il suffit de se connecter à l'adresse : [baker.paris:3000](baker.paris:3000) puis mettre **guest** comme nom d'utilisateur et **guest** comme mot de passe


# Smart rollup

Nous allons à présent créer un smart rollup

Tout d'abord, il faut créer une clef dédiée sur lequel une caution de 10 000ꜩ sera déposée.

```sh
octez-client gen keys scoru
```
J'ai obtenu la clef `tz1Scoruc6TaEyj7QrxmmSg1DLT7DPTYiLpv`
(Pour la regénération, il faut utiliser
```sh
octez-client import secret key scoru unencrypted:edsk3uKSYaH4LBtyWMZrDKEhg1iE9UJnLmmeQxWqfdYjANur8uMYKw
```
)

Tout d'abord, il est possble de déléguer les fonds mis en gage. Nous allons en profiter pour alimenter notre baker :
```sh
octez-client set delegate for scoru to baker
```

On récupère en suite le kernell par défaut

```sh
wget https://tezos.gitlab.io/_downloads/2c4b7ed4990a989aa153d0446433f56e/sr_boot_kernel.sh
source sr_boot_kernel.sh
```

On peut alors originer le rollup :

```sh
octez-client originate smart rollup smart-rollup from scoru of kind wasm_2_0_0 of type bytes with kernel "${KERNEL}" --burn-cap 4
```

l'adresse du rollup ainsi générée est : `sr1SD1Rujzw8S7iUc6vW8hZiXLjKfDpWJ7Ya`

on peut en suite déployer le nœud :
```sh
octez-smart-rollup-node-PtMumbai run operator for sr1SD1Rujzw8S7iUc6vW8hZiXLjKfDpWJ7Ya with operators tz1Scoruc6TaEyj7QrxmmSg1DLT7DPTYiLpv --data-dir /home/ubuntu/scoru
```
