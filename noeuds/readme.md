# exercice des trois nœuds
Pour créer les nœuds, j'ai commencé par définir une procédure pour récupérer un snapshot récent :
```sh
wget https://ghostnet.xtz-shots.io/rolling -O tezos-ghostnet.rolling
```

En suite j'ai défini un fichier yaml qui automatise la création des trois nœuds :

```yaml
session_name: noeuds
start_directory: ~/
before_script: wget https://ghostnet.xtz-shots.io/rolling -O tezos-ghostnet.rolling
windows:
  - window_name: controleur
    layouts:
    panes:
      - shell_command:
         - tail -f noeuds/ghostnet/*/logs
  - window_name: noeud 1
    layout: tiled
    panes:
      - shell_command: # pane no. 1
         - noeud.sh 1
  - window_name: noeud 2
    layout: tiled
    panes:
      - shell_command:
        - noeud.sh 2
  - window_name: noeud 3
    layout: tiled
    panes:
      - shell_command:
         - noeud.sh 3
```

Il suffit alors d'exécuter la commande suivante

```
tmuxp load noeuds.yaml
```
Dès que les nœuds seront lancés, une fenêtre s'ouvriera alors avec les logs des trois nœuds.
