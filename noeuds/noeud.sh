reseau=${2-ghostnet}
num=$1
dir_snapshots=~
dir=/home/ubuntu/noeuds/$reseau/$num
logs=$dir/logs
rpc_client=[::]:973$num
addr=[::]:972$num
metrics=[::]:974$num
mkdir -p $dir_snapshots
mkdir -p $dir


octez-node config init --data-dir=$dir --network $reseau --rpc-addr=$rpc --allow-all-rpc=$rpc --net-addr=$addr --metrics-addr=$metrics
octez-node snapshot import $dir_snapshots/tezos-$reseau.rolling --data-dir=$dir
octez-node run --data-dir=$dir --allow-all-rpc=$rpc --rpc-addr $rpc --log-output=$logs --net-addr=$addr --metrics-addr=$metrics
